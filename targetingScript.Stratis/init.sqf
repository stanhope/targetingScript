
/*Little demonstration.  Spawn a jet, vall the function.  Then sleep half a minute and spawn a hostile that will notice the blufor guy, making the jet attack*/

_spawnPos = [(getPos player1) select 0, (getPos player1) select 1, 500];

_heli = createVehicle ["O_Heli_Light_02_dynamicLoadout_F", _spawnPos, [], 0, "FLY"];
createvehiclecrew _heli;
[_heli, [east, independent], [west], nil, 1000, nil, nil, nil, nil] execVM "targetingScript.sqf";

(allCurators select 0) addCuratorEditableObjects [[_heli, player1],true];

[] spawn {
	sleep 30;
	_group = createGroup east;
	_inf = _group createUnit ["O_G_Soldier_unarmed_F", position player1, [], 0, "NONE"];
	_inf disableAI "PATH";
	(allCurators select 0) addCuratorEditableObjects [[_inf],true];
};
