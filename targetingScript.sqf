/*
Author: stanhope
Description: A script that will make a given airvehicle attack hostile units in a specified range around a given position.

params:
	All but _this select 0 are optional

	(array positions; variable name; type of variable: description)
	_this select 0; _vehicle; 			object: 			The vehicle that is going to attack hostiles.
	_this select 1; _hostileArray; 		array of sides: 	an array of all sides concidered to be allied with the attacking vehicle.  Default: side of driver of _vehicle (_this select 0)
	_this select 2; _friendlyArray; 	array of sides: 	an array of all the sides concidered to be targets of the vehicle.  Default: all sides but those in _hostileArray
	_this select 3; _loiterPos; 		position: 			The position around which the vehicle (_this select 0) should loiter when there are no targets. Default: position of the vehicle upon calling of this script/function.
	_this select 4; _loiterRadius;		distance:			The loiterRange.  Default: 500 meters for helis, 1000 for jets.
	_this select 5; _attackPos;			positions			The position around which the vehicle should attack.  Default: same as _loiterPos
	_this select 6; _attackRange;		distance:			The range in which the vehicle will look for targets to attack. Default: 500 meters for a heli, 1000 for jets.
	_this select 7; _targetlist;		array of typenames:	The types of things the vehicle should attack.  Default: ["Man", "Air", "Car", "Tank"]
	_this select 8; _knowsAboutValue;	number:				Value for the knowsAbout command.  See https://community.bistudio.com/wiki/knowsAbout for more information.  Default: 1.9

License & copyright: check the readme file that will have been provided at the location where you download this script.
*/

/*
Reading params and checking them:

If you are sure that you've entered all the parameters correctly you can remove some of the checks that are being done to verify the params.  
However if you do this and it breaks don't come complaining to me.
*/
params ["_vehicle", "_hostileArray","_friendlyArray","_loiterPos","_loiterRadius","_attackPos","_attackRange",["_targetlist",["Man", "Air", "Car", "Tank"]],["_knowsAboutValue", 1.9]];

_terminalError = false;

/*_this select 0*/
if !(_vehicle isKindOf "Air") then { 
	diag_log "targetingScript.sqf: error: param 0 (_vehicle) is not kind of air. The script is for airvehicles only."; 
	_terminalError = true;
};

private _pilot = driver _vehicle;
if (isNil "_pilot") then { 
	diag_log "targetingScript.sqf: error: vehicle has no driver.";
	_terminalError = true;
};

/*_this select 1*/
switch true do {
	case (isNil "_hostileArray"): {
		_hostileArray = [(side _pilot)];
	};
	case (typeName _hostileArray != "ARRAY"): {
		diag_log "targetingScript.sqf: warning: _hostileArray is not an array, reverting back to default value.";
		_hostileArray = [(side _pilot)];
	};
	case (typeName (_hostileArray select 0) != "SIDE"): {
		diag_log "targetingScript.sqf: warning: _hostileArray select 0 is not a side, reverting back to default value.";
		_hostileArray = [(side _pilot)];
	};
	default {};
};

/*_this select 2*/
private _defaultValueGetter = {
	_friendlyArray = [blufor, opfor , independent, civilian];
	{
		if (_x in _hostileArray) then {
			_friendlyArray = _friendlyArray - [_x];
		};
	} forEach _friendlyArray;
	
	if (count _friendlyArray == 0) then {
		diag_log "targetingScript.sqf: error: could not assign default value to _friendlyArray.";
		_terminalError = true;
	};
};
private _defaultValueScript = scriptNull;
switch true do {
	case (isNil "_friendlyArray"): {
		_defaultValueScript = [] spawn _defaultValueGetter;
	};
	case (typeName _friendlyArray != "ARRAY"): {
		diag_log "targetingScript.sqf: warning: _friendlyArray is not an array, reverting back to default value.";
		_defaultValueScript = [] spawn _defaultValueGetter;
	};
	case (typeName (_friendlyArray select 0) != "SIDE"): {
		diag_log "targetingScript.sqf: warning: _friendlyArray select 0 is not a side, reverting back to default value.";
		_defaultValueScript = [] spawn _defaultValueGetter;
	};
	default {};
};
waitUntil {sleep 0.1; scriptDone _defaultValueScript};

/*_this select 3*/
switch true do {
	case (isNil "_loiterPos"): {
		_loiterPos = getPos _vehicle;
	};
	case (typeName _loiterPos != "ARRAY"): {
		diag_log "targetingScript.sqf: warning: _loiterPos is not an array, reverting back to default value.";
		_loiterPos = getPos _vehicle;
	};
	case (typeName (_loiterPos select 0) != "SCALAR"): {
		diag_log "targetingScript.sqf: warning: _loiterPos select 0 is not a number, reverting back to default value.";
		_loiterPos = getPos _vehicle;
	};
	default {};
};

/*_this select 4*/
private _defaultValueGetter = {
	if (_vehicle isKindOf "Helicopter") then {
		_loiterRadius = 500;
	} else {
		_loiterRadius = 1000;
	};
};
private _defaultValueScript = scriptNull;

switch true do {
	case (isNil "_loiterRadius"): {
		_defaultValueScript = [] spawn _defaultValueGetter;
	};
	case (typeName _loiterRadius != "SCALAR"): {
		diag_log "targetingScript.sqf: warning: _loiterRadius is not a number, reverting back to default value.";
		_defaultValueScript = [] spawn _defaultValueGetter;
	};
	default {};
};
waitUntil {sleep 0.1; scriptDone _defaultValueScript};

/*_this select 5*/
switch true do {
	case (isNil "_attackPos"): {
		_attackPos = _loiterPos;
	};
	case (typeName _attackPos != "ARRAY"): {
		diag_log "targetingScript.sqf: warning: _attackPos is not an array, reverting back to default value.";
		_attackPos = _loiterPos;
	};
	case (typeName (_attackPos select 0) != "SCALAR"): {
		diag_log "targetingScript.sqf: warning: _attackPos select 0 is not a number, reverting back to default value.";
		_attackPos = _loiterPos;
	};
	default {};
};

/*_this select 6*/
switch true do {
	case (isNil "_attackRange"): {
		_attackRange = _loiterRadius;
	};
	case (typeName _attackRange != "SCALAR"): {
		diag_log "targetingScript.sqf: warning: _loiterRadius is not a number, reverting back to default value.";
		_attackRange = _loiterRadius;
	};
	default {};
};

/*_this select 7*/
switch true do {
	case (typeName _targetlist != "ARRAY"): {
		diag_log "targetingScript.sqf: warning: _targetlist is not an array, reverting back to default value.";
		_targetlist = ["Man", "Air", "Car", "Tank"];
	};
	case (typeName (_targetlist select 0) != "STRING"): {
		diag_log "targetingScript.sqf: warning: _loiterPos select 0 is not string, reverting back to default value.";
		_targetlist = ["Man", "Air", "Car", "Tank"];
	};
	default {};
};

/*_this select 8*/
if (typeName _knowsAboutValue != "SCALAR") then { 
	diag_log "targetingScript.sqf: warning: _knowsAboutValue is not a number, reverting back to default value.";
	_knowsAboutValue = 1.9;
};

/*check if the script should terminate*/
if (_terminalError) exitWith {
	hint "targetingScript.sqf encountered a terminal error and exited.";
	diag_log "targetingScript.sqf: terminal error";
};

/*Actual script:*/
private _vehicleGrp = group _pilot;

/*setup the loiter first*/
while {(count (waypoints _vehicleGrp)) > 0} do{
	deleteWaypoint ((waypoints _vehicleGrp) select 0);
};

private _vehicleWp = _vehicleGrp addWaypoint [_loiterPos, 0];
_vehicleWp setWaypointType "LOITER";
_vehicleWp setWaypointLoiterRadius (_loiterRadius);
_vehicle flyInHeight (250 + random 250);
_vehicle limitSpeed 375;


/*core loop*/
while {alive _vehicle} do {

	//reset some things
	private _accepted = false;
	private _target = objNull;
	
	//Find someone or something to attack
	while {!_accepted} do {
	
		/*just to be sure that our vehicle is still alive*/
		if (!alive _vehicle) exitWith {};
		
		/*Sleep a bit to not spam the targeting loop*/
		sleep 5;
	
		/*Get all targets of the right type that are in range.*/
		private _targetList = _attackPos nearEntities [_targetlist, _attackRange];

		/*count all viable targets*/
		if ( (count _targetList) > 0) then {
			/*select a random target and perform additional checks*/
			_target = selectRandom _targetList;
			
			switch true do {
				/*Because arma*/
				case (_target isEqualTo objNull): { _accepted = false;};
				
				/*check if we didn't select a friendly*/
				case ( not((side _target) in _friendlyArray) ): { _accepted = false;};
				
				/*check if we know about this target*/
				default {
					{
						if ((_x knowsAbout _target) > _knowsAboutValue) then {
							_accepted = true;
						};
					} forEach _hostileArray;
				};
			};
		} else {
			/*no viable targets are found, sleep for 5 seconds and look again.*/
			_accepted = false;
		};
		/*looptimeout*/
		sleep 0.1;
	};
	
	
	_vehicleGrp reveal [_target,4];
	if (!alive _vehicle) exitWith {};
	
	//delete the current waipoints of the vehicle
	while {(count (waypoints _vehicleGrp)) > 0} do{
		deleteWaypoint ((waypoints _vehicleGrp) select 0);
	};
	
	//Attack the target:
	private _laser = "LaserTargetW" createVehicle (getPos _target);
	_laser attachTo [_target,[0,0,0]];
	_vehicle doTarget _target;
	_vehicle doFire _target;
	[_vehicleGrp,(getPos _target)] call BIS_fnc_taskAttack;
	[_vehicleGrp, 0] waypointAttachObject _target;
	
	
	//Let him continiue attacking for 2 minutes or until the target is dead
	_i = 0;
	while {alive _target && alive _vehicle && _i < 120} do {
		sleep 5;
		_i = _i + 5;
		_vehicleGrp reveal [_target,4];
		_vehicle doTarget _target;
		_vehicle doFire _target;
	};
	
	//delete the current waipoints of the jet and the laser:
	deleteVehicle _laser;
	while {(count (waypoints _vehicleGrp)) > 0} do{
		deleteWaypoint ((waypoints _vehicleGrp) select 0);
	};
	
	if (!alive _vehicle) exitWith {};
	
	//let him loiter for 2 minutes then attack the next thing
	private _vehicleWp = _vehicleGrp addWaypoint [_loiterPos, 0];
	_vehicleWp setWaypointType "LOITER";
	_vehicleWp setWaypointLoiterRadius (_loiterRadius);
	_vehicle flyInHeight (250 + random 250);
	_vehicle limitSpeed 375;
	sleep 120;
};
